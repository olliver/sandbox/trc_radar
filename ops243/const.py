from __future__ import annotations
from typing import Final

DEFAULT_SERIAL: Final = "/dev/ttyOPS24x"
DEFAULT_BAUD: Final = 19200

MAX_RANGE: Final = 300

MAJOR_VERSION: Final = 0
MINOR_VERSION: Final = 0
PATCH_VERSION: Final = 1
__version__: Final = f"{MAJOR_VERSION}.{MINOR_VERSION}.{PATCH_VERSION}"
