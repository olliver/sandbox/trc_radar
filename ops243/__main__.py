"""OmniPresense Sensor 24x distance sensor"""

from __future__ import annotations

import argparse
import json
import shutil
import sys

import serial

from const import __version__, DEFAULT_BAUD, DEFAULT_SERIAL, MAX_RANGE


def init_sensor(args: argparse.Namespace) -> serial.serialposix.Serial:
    """Initializes the serial connected sensor"""
    ser = serial.Serial(
        port = args.port,
        baudrate = args.baud,
        parity = serial.PARITY_NONE,
        stopbits = serial.STOPBITS_ONE,
        bytesize = serial.EIGHTBITS,
        timeout = 1,
        writeTimeout = 2,
    )
    ser.flushInput()
    ser.write(b"OJ")              # Output in JSON format
    ser.write(b"OD")              # Output range data
    ser.write(b"Os")              # Do not output speed data
    ser.write(b"om")              # Do not output magnitude
    ser.write(b"uC")              # Range in cm per second
    ser.write(b"F0")              # 0 Decimal places on range
    ser.write(b"SC")              # 100.000 Sampling frequency
    ser.write(b"S>")              # 1024 buffer size
    ser.write(b"r>0\n")           # Minimal range to start reporting, 0 cm
    ser.write(b"r<0\n")           # Maximum range to stop reporting
    ser.flushOutput()

    return ser


def get_arguments() -> argparse.Namespace:
    """CLI argument parsing"""
    parser = argparse.ArgumentParser(
        description="Omnipresense OPS243 Sensor parser",
    )

    parser.add_argument("-v", "--version", action="version", version=__version__)
    parser.add_argument(
        "-p",
        "--port",
        action = "store",
        default = DEFAULT_SERIAL,
        help="Device port where to find sensor",
    )
    parser.add_argument(
        "-b",
        "--baud",
        action = "store",
        default = DEFAULT_BAUD,
        help="Baud rate of the serial port",
    )

    return parser.parse_args()


def main() -> int:
    """Main function"""
    args = get_arguments()
    ser = init_sensor(args)

    # Read input forever
    while True:
        screenwidth = shutil.get_terminal_size((80, 20)).columns
        line = b""
        while not line.startswith(b"{"):
            line = ser.readline().replace(b"\x00", b"")

        opsjson = json.loads(line)
        if "range" in opsjson:
            distance = int(opsjson["range"])
            markers = int(((distance * screenwidth) / MAX_RANGE) - 40)

            marker = "Range: " +  "#" * markers if markers < screenwidth else (screenwidth - 40)
            ranger = f"{distance:<20}{marker}"
            ranger += " " * int(screenwidth - len(ranger))

            print(ranger, end="\r")

    return 0

if __name__ == "__main__":
    sys.exit(main())
